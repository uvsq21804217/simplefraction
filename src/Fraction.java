
public class Fraction {
	private double d;

	public Fraction(double d) {
		this.d = d;
	}

	@Override
	public String toString() {
		return "Fraction : " + this.getD();
	}

	public double getD() {
		return d;
	}

	public void setD(double d) {
		this.d = d;
	}
	
}
